package com.example.creation;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public class MainActivity extends Activity {
  <caret>
  private GoogleApiClient client;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
  }

  @Override
  public void onStart() {
    super.onStart();

    Action viewAction = Action.newAction(
        Action.TYPE_VIEW,
        "Main Page",
        Uri.parse("http://www.example.com/main"),
        Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    int b = 1;
    switch(b) {
      case 1:
        b ++;
      default:
        PendingResult<Status> a = AppIndex.AppIndexApi.start(client, viewAction);
        client.connect();
    }
  }

  @Override
  public void onStop() {
    super.onStop();

    Action viewAction = Action.newAction(
        Action.TYPE_VIEW,
        "Main Page",
        Uri.parse("http://www.example.com/main"),
        Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    int b = 1;
    boolean c= true;
    try {
    } catch (Exception e) {
    } finally {
      while (b-- > 0)
        if (c) {
          AppIndex.AppIndexApi.end(client, viewAction);
        }
      client.disconnect();
    }
  }
}
